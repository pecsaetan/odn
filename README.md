# :world_map: Open Delivery Network
The Open Delivery Network is a free and public file distribution network for open-source projects operated and maintained by Pecsætan. It is somewhat similar to other public CDNs such as [cdnjs](https://cdnjs.org) and the [Google CDN](https://developers.google.com/speed/libraries/?csw=1). From the Amercias through Europe and to Asia, we focus on delivering globally.

:earth_americas: :earth_africa: :earth_asia: